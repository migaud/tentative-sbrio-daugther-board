EESchema Schematic File Version 4
LIBS:SBRio_Expansion_Board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:DB37_Female J?
U 1 1 5E4AD5C1
P 1800 3000
F 0 "J?" H 1720 883 50  0000 C CNN
F 1 "DB37_Female" H 1720 974 50  0000 C CNN
F 2 "" H 1800 3000 50  0001 C CNN
F 3 " ~" H 1800 3000 50  0001 C CNN
	1    1800 3000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 5E4AD677
P 8000 4700
F 0 "R?" V 7793 4700 50  0000 C CNN
F 1 "1.7k" V 7884 4700 50  0000 C CNN
F 2 "" V 7930 4700 50  0001 C CNN
F 3 "~" H 8000 4700 50  0001 C CNN
	1    8000 4700
	0    1    1    0   
$EndComp
Text Label 2100 4800 0    50   ~ 0
AIR_commande
Text Label 7150 4700 0    50   ~ 0
AIR_commande
Wire Wire Line
	7150 4700 7850 4700
$Comp
L Device:Q_NMOS_DGS Q?
U 1 1 5E4ADBE0
P 8350 4700
F 0 "Q?" H 8555 4746 50  0000 L CNN
F 1 "Q_NMOS_DGS" H 8555 4655 50  0000 L CNN
F 2 "" H 8550 4800 50  0001 C CNN
F 3 "~" H 8350 4700 50  0001 C CNN
	1    8350 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E4ADD27
P 8450 4900
F 0 "#PWR?" H 8450 4650 50  0001 C CNN
F 1 "GND" H 8455 4727 50  0000 C CNN
F 2 "" H 8450 4900 50  0001 C CNN
F 3 "" H 8450 4900 50  0001 C CNN
	1    8450 4900
	1    0    0    -1  
$EndComp
$Comp
L Relay:AZ850-x K?
U 1 1 5E4ADF9E
P 8850 4200
F 0 "K?" H 9480 4246 50  0000 L CNN
F 1 "AZ850-x" H 9480 4155 50  0000 L CNN
F 2 "Relay_THT:Relay_DPDT_FRT5" H 9400 4250 50  0001 C CNN
F 3 "http://www.azettler.com/pdfs/az850.pdf" H 8650 4200 50  0001 C CNN
	1    8850 4200
	1    0    0    -1  
$EndComp
Text Label 8450 3900 0    50   ~ 0
24V
Text Label 2100 4600 0    50   ~ 0
AIR_sensing
$Comp
L Device:R R?
U 1 1 5E4BD894
P 9350 3300
F 0 "R?" H 9420 3346 50  0000 L CNN
F 1 "4.6k" H 9420 3255 50  0000 L CNN
F 2 "" V 9280 3300 50  0001 C CNN
F 3 "~" H 9350 3300 50  0001 C CNN
	1    9350 3300
	1    0    0    -1  
$EndComp
Text Label 9350 3150 0    50   ~ 0
5V
Text Label 9600 3450 0    50   ~ 0
AIR_sensing
Wire Wire Line
	9350 3450 9600 3450
Connection ~ 9350 3450
$Comp
L power:GND #PWR?
U 1 1 5E4BD961
P 9250 4500
F 0 "#PWR?" H 9250 4250 50  0001 C CNN
F 1 "GND" H 9255 4327 50  0000 C CNN
F 2 "" H 9250 4500 50  0001 C CNN
F 3 "" H 9250 4500 50  0001 C CNN
	1    9250 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 3450 9350 3900
$EndSCHEMATC
